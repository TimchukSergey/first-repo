package by.shag.timchuk.service;

import by.shag.timchuk.api.dto.StudentDto;
import by.shag.timchuk.api.dto.StudentDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private static Integer idSequence = 5;
    private List<StudentDto> studentDtoList;

    public StudentService() {
        this.studentDtoList = new ArrayList<>();
        studentDtoList.add(new StudentDto(1, "Dima", "DimaDima", 1000));
        studentDtoList.add(new StudentDto(2, "Anton", "AntonAnton", 2000));
        studentDtoList.add(new StudentDto(3, "Vlad", "VladVlad", 3000));
        studentDtoList.add(new StudentDto(4, "Sergei", "SergeiSergei", 4000));
    }

    public StudentDto createStudent(StudentDto dto) {//т.к. создано уже 4 студента
        dto.setId(idSequence++);
        studentDtoList.add(dto);
        return dto;
    }

     public List<StudentDto> findByAll() {
        return studentDtoList;
    }

    public StudentDto findById(Integer id) {
        Optional<StudentDto> desiredStudent = studentDtoList.stream()
                .filter(student -> student.getId().equals(id))
                .findFirst();

        return desiredStudent.orElseThrow(() -> new RuntimeException("Not found"));
    }

    public StudentDto updateStudentById(StudentDto studentDto) {
        StudentDto persistedStudent = findById(studentDto.getId());

        persistedStudent.setName(studentDto.getName());
        persistedStudent.setLastname(studentDto.getLastname());
        persistedStudent.setYearOfBirth(studentDto.getYearOfBirth());
        return persistedStudent;
    }

    public void deleteStudentById(Integer id) {
        studentDtoList.remove(findById(id));// сравнивает на equals и hashCode (надо переопределить)
    }



}
