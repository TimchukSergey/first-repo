package by.shag.timchuk.api;

import by.shag.timchuk.api.dto.StudentDto;
import by.shag.timchuk.service.StudentService;
import by.shag.timchuk.api.dto.StudentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/student")
    public String findAll(Model model) {
        List<StudentDto> all = studentService.findByAll();
        model.addAttribute("students", all);
        return "students.html";
    }

    @GetMapping(value = "/student/new-student")
    public String getCreateForm(Model model) {
        model.addAttribute("student", new StudentDto());
        return "student-create.html";
    }

    @PostMapping("/student")
    public String createStudentReturnAll(
            @ModelAttribute("student") StudentDto studentDto, Model model) {
        studentService.createStudent(studentDto);
        return findAll(model);
    }

    @DeleteMapping("/student")
    public String deleteStudentReturnAll(@ModelAttribute("student") Integer id, Model model) {
        studentService.deleteStudentById(id);
        return findAll(model);
    }
}
