package by.shag.timchuk.api;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String getLoginForm() {
        return "login.html";
    }


    @PostMapping("/login")
    public String login(@RequestParam(value = "login", required = true) String login,
                        @RequestParam(value = "password", required = true) String password,
                        Model model){
        if (login.equals("admin") && password.equals("admin")) {
            model.addAttribute("name2",login);
            model.addAttribute("name3",login);
            return "greeting.html";
        } else {
            return "login.html";
        }
    }

}
