package by.shag.timchuk.api;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@Controller
//public class HelloWorldController {
//
//    @GetMapping("/greeting")
//    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
//        model.addAttribute("name", name);
//        return "greeting";
//    }
//
//    @GetMapping("/greeting0")
//    public String greeting0(
//            @RequestParam(name = "name") String name,
//            @RequestParam(name = "lastname") String lastname,
//            @RequestParam(name = "age") Integer age
//    ) {
//
//        return "helloworld.html";
//    }
//
//    @GetMapping("/greeting2")
//    public String greeting2() {
//        return "helloworld.html";
//    }
//
//    @GetMapping("/home")
//    public String home() {
//        return "home.html";
//    }
//
//}


@Controller
public class HelloWorldController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/greeting2")
    public String greeting2(){
        return "helloworld.html";
    }

    @GetMapping("/home")
    public String home(){
        return "home.html";
    }

    @GetMapping("/greeting0")
    public String greeting0(@RequestParam(name="name", required=false) String name) {
        return "helloworld.html";
    }
}

//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@Controller
//public class HelloWorldController {
//
//    @GetMapping("/greeting")
//    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name,
//                           Model model) {
//        model.addAttribute("name", name);
//        return "greeting";
//    }
//
//    @GetMapping("/greeting2")
//    public String greeting2() {
//                return "helloworld.html";
//    }
//
//    @GetMapping("/home")
//    public String home() {
//        return "home.html";
//    }
//
//
//    @GetMapping("/greeting0")
//    public String greeting0(@RequestParam(name="name", required=false) String name)
//    {return "helloworld.html";}
//
//
//    @GetMapping("/greeting0")
//    public String greeting0(
//            @RequestParam(name="name") String name,
//            @RequestParam(name="lastname") String lastname,
//            @RequestParam(name="age") Integer age
//    ) {
//        System.out.println(name + lastname + age);
//        return "helloworld.html";
//    }

//
//    //домашка
//    @GetMapping("/sum")
//    public String sum(
//            @RequestParam(name="first") Double first,
//            @RequestParam(name="second", required=false, defaultValue="0") Integer second
//    ) {
//        double sum = first + second;
//        System.out.println("Summa = " + String.format("%.2f",sum));
//        return "helloworld.html";
//    }

//}