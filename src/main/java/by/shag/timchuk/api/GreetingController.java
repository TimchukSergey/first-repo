package by.shag.timchuk.api;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping(value = "/simpleGreeting")
    public String simpleGreeting(@RequestParam(value = "name", required = true) String name,
                                 @RequestParam(value = "surname", required = true) String surName,
                                 Model model) {
        model.addAttribute("name2", name);
        model.addAttribute("name3", surName);
        return "greeting.html";
    }



}
